﻿namespace DlegatesAndEvents
{
    public class Command
    {
        /// <summary>
        /// Идентификатор команды.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование команды.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Количество баллов команды.
        /// </summary>
        public float Points { get; set; }
    }
}
