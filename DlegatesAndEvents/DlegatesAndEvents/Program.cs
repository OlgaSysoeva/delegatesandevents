﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DlegatesAndEvents
{
    class Program
    {
        private static FindFiles _fileFound;

        static void Main(string[] args)
        {
            // поиск максимального элемента коллекции
            TestGetMaxValue();

            // поиск файлов в каталоге
            TestFindFiles();
        }

        /// <summary>
        /// Метод запускает поиска максимального элемента коллекции.
        /// </summary>
        static void TestGetMaxValue()
        {
            IEnumerable<Command> commands = new List<Command>()
            {
                new Command() { Id = 1, Name = "Спортсмены", Points = 5.60f },
                new Command() { Id = 2, Name = "Медведи", Points = 8.20f },
                new Command() { Id = 3, Name = "Древние греки", Points = 3.70f }
            };

            Console.WriteLine("Команды участники:");

            foreach (var item in commands)
            {
                Console.WriteLine($"Номер: {item.Id} Название: {item.Name} Количество очков: {item.Points}");
            }

            var winner = commands.GetMax<Command>(x => x.Points);

            Console.WriteLine($"\nПобедила команда: {winner?.Name}");
        }

        /// <summary>
        /// Метод запуска поиска файлов в каталоге
        /// </summary>
        static void TestFindFiles()
        {
            Console.WriteLine("\nУкажите каталог поиска файлов: ");
            string path = Console.ReadLine();

            if (string.IsNullOrEmpty(path) || !Directory.Exists(path))
            {     
                Console.WriteLine("Некорректный каталог!");
                return;
            }

            _fileFound = new FindFiles(path);
            _fileFound.FileFound += WriteMessage;

            Console.WriteLine("\nНажмите 'n' если желаете остановить поиск!\n");

            _fileFound.Start();
        }

        /// <summary>
        /// Метод выводит на консоль полное имя файла.
        /// </summary>
        /// <param name="sender">Объект, в котором вызывается метод.</param>
        /// <param name="e">Класс с данными для вывода.</param>
        static void WriteMessage(object sender, FileArgs e)
        {
            Console.WriteLine($"Найден файл: {e.FileName}");
            
            // возможность прекратить поиск
            if (Console.ReadKey(true).KeyChar == 'n'|| Console.ReadKey(true).KeyChar == 'N')
                _fileFound.FileFound -= WriteMessage;
        }
    }
}
