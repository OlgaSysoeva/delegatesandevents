﻿using System;

namespace DlegatesAndEvents
{
    /// <summary>
    /// Класс для хранения имени файла.
    /// </summary>
    public class FileArgs : EventArgs
    {
        /// <summary>
        /// Полное имя файла.
        /// </summary>
        public string FileName { get; set; }
    }
}
