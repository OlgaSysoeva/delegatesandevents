﻿using System;
using System.IO;

namespace DlegatesAndEvents
{
    /// <summary>
    /// Класс для поиска файлов в каталоге.
    /// </summary>
    public class FindFiles
    {
        /// <summary>
        /// Полный путь к каталогу для поиска.
        /// </summary>
        private string _path;

        public FindFiles(string path)
        {
            _path = path;
        }

        /// <summary>
        /// Метод запуска поиска файлов в каталоге.
        /// </summary>
        public void Start()
        {
            string[] fileEntries = Directory.GetFiles(_path);
            
            foreach (string fileName in fileEntries)
            {
                FileArgs args = new FileArgs
                {
                    FileName = fileName
                };

                // Вызов события
                FileFound?.Invoke(this, args);
            };
        }

        /// <summary>
        /// Событие, возникающее при нахождении файла.
        /// </summary>
        public event EventHandler<FileArgs> FileFound;
    }
}
