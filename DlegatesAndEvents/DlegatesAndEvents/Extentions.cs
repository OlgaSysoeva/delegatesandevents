﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DlegatesAndEvents
{
    public static class Extentions
    {
        /// <summary>
        /// Метод получает максимальный элемент коллекции по предикату.
        /// </summary>
        /// <typeparam name="T">Тип параметра - любой класс.</typeparam>
        /// <param name="items">Коллекция классов.</param>
        /// <param name="predicate">Предикат, по которому осуществляется поиск.</param>
        /// <returns>Возвращает элемент коллекции с максимальным предикатом.</returns>
        public static T GetMax<T>(this IEnumerable<T> items, Func<T, float> predicate) where T : class
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items)); 

            if (!items.Any())
                return null;

            var maxValue = items.Max(predicate);

            return items.FirstOrDefault(x => predicate(x) == maxValue);
        }
    }
}
